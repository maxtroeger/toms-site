#!/bin/sh

dir="$HOME/Internet/personal-site"
blogfile="blog/index.html"

if [ -z "$1" ]; then
	echo "Title of post:"
	read -r title
	[ -z "$title" ] && exit
	title_trim=$(echo "$title" | sed 's/\?//g;s/\!//g;s/\.//g;s/\,//g;s/  */-/g;s/$/.html/' | tr '[:upper:]' '[:lower:]')
	echo "\
<!DOCTYPE html>
<html lang=\"en\">
<head>
<title>$title</title>
<meta charset=\"utf-8\"/>
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
<link rel=\"alternative\" type=\"application/rss+xml\" title=\"RSS\" href=\"https://tomfasano.xyz/rss.xml\" />
<link rel=\"stylesheet\" type=\"text/css\" href=\"../style.css\">
<link rel=\"icon\" href=\"../favicon.ico\">
</head>
<nav>
	<a href=\"/\">Home</a>
	<a href=\"../posts.html\">Posts</a>
	<span class=\"post\">$(date '+%Y-%m-%d')</span>
</nav>
<body>
<h1>$title</h1>
</body>
</html>" > "$dir/posts/$title_trim"
	echo "$title_trim created and placed in $dir/posts" && exit
fi

if [ -z "$2" ]; then
	link="posts"
else
	link="$2"
fi
link="$link/$(basename "$1")"
[ ! -f "$dir/$link" ] && echo "$1 not in $dir/posts. Exiting." && exit 1
title="$(sed -n 's/<title>\(.*\)<\/title>/\1/p' "$1")"
grep "$title" "$dir/$blogfile" && echo "$1 has already been added to $blogfile. Exiting." && exit 1

entrydate=$(date '+%m/%d')
temp="$(mktemp)";
trap 'rm -f "$temp"' 0 1 2 3 15
echo "\
	<li>$entrydate &ndash; <a href=\"/$link\">$title</a></li>" > "$temp"

# Appends new post to blog index

sed -i "/<!--insert-->/r $temp" "/$dir/$blogfile"
